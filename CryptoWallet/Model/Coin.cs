﻿using CryptoWallet.Model.Exchanges;
using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoWallet.Model
{
    public class Coin : ObservableObject
    {
        public Coin() { }

        public static EventHandler EquivalentInCurrencyChangedEvent;

        public Coin(string title, Exchange exchange)
        {
            Title = title;
            Exchange = exchange;
        }

        public Coin(string title, double amount, Exchange exchange)
        {
            Title = title;
            Amount = amount;
            Exchange = exchange;
        }

        private Exchange _exchange;
        public Exchange Exchange {
            get { return _exchange; }
            private set {
                _exchange = value;
                RaisePropertyChanged(() => Exchange);
            }
        }

        private string _title;
        public string Title {
            get { return _title; }
            set {
                _title = value;
                RaisePropertyChanged(() => Title);
            }
        }

        private string _fullTitle;
        public string FullTitle {
            get { return _fullTitle; }
            set {
                _fullTitle = value;
                RaisePropertyChanged(() => FullTitle);
            }
        }

        private double _sellPrice;
        public double SellPrice {
            get { return _sellPrice; }
            set {
                _sellPrice = value;

                EquivalentInCurrency = _amount * SellPrice;

                RaisePropertyChanged(() => SellPrice);
            }
        }

        private double _amount;
        public double Amount {
            get { return _amount; }
            set {
                _amount = value;

                EquivalentInCurrency = _amount * SellPrice;

                RaisePropertyChanged(() => Amount);
            }
        }

        private double _equivalentInCurrency;
        public double EquivalentInCurrency {
            get { return _equivalentInCurrency; }
            set {
                _equivalentInCurrency = value;

                RaisePropertyChanged(() => EquivalentInCurrency);
                EquivalentInCurrencyChangedEvent?.Invoke(null, EventArgs.Empty);
            }
        }

        public static explicit operator CoinToStore(Coin coin) =>
            new CoinToStore(coin.Exchange.Name, coin.Title, coin.Amount);
    }
}
