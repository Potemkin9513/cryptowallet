﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoWallet.Model
{
    [Serializable]
    public class CoinToStore
    {
        public CoinToStore(String exchangeName, String title, Double amount)
        {
            ExchangeName = exchangeName;
            Title = title;
            Amount = amount;
        }

        public String ExchangeName { get; set; }
        public String Title { get; set; }
        public Double Amount { get; set; }
    }
}
