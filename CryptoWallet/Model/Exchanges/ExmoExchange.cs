﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace CryptoWallet.Model.Exchanges
{
    public class ExmoExchange : Exchange
    {
        private const String _url = "http://api.exmo.com/v1/{0}";

        public override String Name { get; } = "Exmo";

        protected override IDictionary<String, String> CurrencyPairs { get; set; }

        public ExmoExchange() : base()
        {
        }

        protected override void SetCurrencyPairs()
        {
            CurrencyPairs = new Dictionary<String, String> {
                { "rub", "RUB" },
                { "usd", "USD" },
                { "btc", "BTC" }
            };

            bool isWrongCurrency =
                CurrencyPairs.Any(title => !CurrencyStringList.Contains(title.Key));
            bool isAllExists = CurrencyStringList
                .ToList()
                .TrueForAll(
                    currency => CurrencyPairs
                    .Select(pair => pair.Key)
                    .Contains(currency)
                );
            if (CurrencyPairs.Count != CurrencyStringList.Count || isWrongCurrency || !isAllExists)
                throw new Exception();
        }

        protected override void FillCoinsPrices()
        {
            var apiName = "ticker";

            var text = GetHttpRequestText(String.Format(_url, apiName));

            var result = JsonConvert.DeserializeObject<Dictionary<String, object>>(text);


            var currency = CurrencyPairs[CoinsManager.SelectedCurrencyCommonTitle];
            var coinsTitles = Coins.Select(coin => $"{coin.Title}_{currency}").ToArray();
            var pairs = JsonConvert.DeserializeObject<Dictionary<String, JToken>>(text)
                .Where(pair => coinsTitles.Contains(pair.Key));
            var separator = Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator;
            foreach (var coin in pairs)
            {
                var parameters = JsonConvert.DeserializeObject<Dictionary<String, String>>(coin.Value.ToString());

                var sellPrice = Convert.ToDouble(parameters["sell_price"].Replace(".", separator));
                var title = coin.Key.Split('_')[0];
                Coins.ToList().Find(c => c.Title == title).SellPrice = sellPrice;
            }

            CalculateExchangeBalance();
        }

        protected override List<KeyValuePair<String, String>> GetAllPairs()
        {
            var apiName = "pair_settings";
            var text = GetHttpRequestText(String.Format(_url, apiName));

            JToken a = (JToken)JsonConvert.DeserializeObject(text);
            Dictionary<String, JToken> pairInfo =
                JsonConvert.DeserializeObject<Dictionary<String, JToken>>(a.ToString());

            List<KeyValuePair<String, String>> pairs = new List<KeyValuePair<String, String>>();

            foreach (var pair in pairInfo)
            {
                var coinInPair = pair.Key.Split('_');
                pairs.Add(new KeyValuePair<String, String>(coinInPair[0], coinInPair[1]));
            }

            return pairs;
        }
    }
}
