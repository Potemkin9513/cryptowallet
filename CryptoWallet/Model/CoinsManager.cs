﻿using CryptoWallet.Model;
using CryptoWallet.Model.Exchanges;
using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoWallet.Model
{
    public class CoinsManager : ObservableObject
    {
        public ObservableCollection<Coin> Coins { get; set; } 
            = new ObservableCollection<Coin>();

        public ObservableCollection<Exchange> Exchanges { get; set; } 
            = new ObservableCollection<Exchange>();

        private readonly IOService _ioService;

        private static EventHandler _selectedCurrencyChanged;

        public CoinsManager(IOService ioService)
        {
            _ioService = ioService;

            var yobitExchange = new YobitExchange();
            var exmoExchange = new ExmoExchange();
            Exchanges.Add(new YobitExchange());
            Exchanges.Add(new ExmoExchange());

            Exchanges.ToList()
                .ForEach(e => e.ExchangeBalanceChanged += (s, ea) => CalculateExchangeBalance());

            _selectedCurrencyChanged += (s, e) => FillAllCoinsPricesWithLoading();

            SelectedCurrencyCommonTitle = Exchange.CurrencyStringList[0];
        }

        private static string _selectedCurrencyCommonTitle;
        public static string SelectedCurrencyCommonTitle {
            get { return _selectedCurrencyCommonTitle; }
            set {
                if (_selectedCurrencyCommonTitle == value) {
                    return;
                }

                _selectedCurrencyCommonTitle = value;

                _selectedCurrencyChanged?.Invoke(null, EventArgs.Empty);
            }
        }

        private double _totalBalance;
        public double TotalBalance {
            get { return _totalBalance; }
            set {
                _totalBalance = value;
                RaisePropertyChanged(() => TotalBalance);
            }
        }

        private void CalculateExchangeBalance()
        {
            TotalBalance = Exchanges.Sum(e => e.ExchangeBalance);
        }

        public void FillAllCoinsPricesWithLoading()
        {
            if (Coins.Count == 0)
                return;

            Task.Run(() => {
                foreach (var exchange in Exchanges) {
                    exchange.FillCoinsPricesWithLoading();
                }
            });
        }

        public void RemoveCoin(Coin coin)
        {
            Coins.Remove(coin);
            coin.Exchange.RemoveCoin(coin);
        }

        public void AddCoin(string coinTitle, string exchangeName)
        {
            var exchange = Exchanges.ToList().Find(e => e.Name == exchangeName);

            if (exchange == null) {
                throw new ArgumentException("Invlalid exchange name!");
            }

            var newCoin = new Coin(coinTitle, exchange);

            Coins.Add(newCoin);
            exchange.AddCoin(newCoin);
        }

        private void AddCoin(string coinTitle, string exchangeName, double amount)
        {
            var exchange = Exchanges.ToList().Find(e => e.Name == exchangeName);

            if (exchange == null) {
                throw new ArgumentException("Invlalid exchange name!");
            }

            var newCoin = new Coin(coinTitle, amount, exchange);

            Coins.Add(newCoin);
            exchange.AddCoin(newCoin);
        }

        public void Write()
        {
            var coinsToStore = Coins.Select(c => (CoinToStore)c);

            _ioService.Write(coinsToStore);
        }

        public void Read()
        {
            var coinsToStore = _ioService.Read();

            Coins.Clear();
            Exchanges.ToList().ForEach(e => e.Coins.Clear());

            foreach (var coin in coinsToStore) {
                AddCoin(coin.Title, coin.ExchangeName, coin.Amount);
            }
        }
    }
}
