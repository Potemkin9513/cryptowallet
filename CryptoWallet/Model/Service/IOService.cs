﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoWallet.Model
{
    public interface IOService
    {
        void Write(IEnumerable<CoinToStore> coins);
        IEnumerable<CoinToStore> Read();
    }
}
