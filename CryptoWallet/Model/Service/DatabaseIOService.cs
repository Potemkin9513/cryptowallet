﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace CryptoWallet.Model.Service
{
    internal class DatabaseIOService : IOService
    {
        private const String _getBalanceProcName = "sp_GetBalance";
        private const String _sqlInsertExpression = "sp_AddCoin";
        private readonly String _connectionString;

        public DatabaseIOService(String connectionString)
        {
            _connectionString = connectionString;
        }

        public IEnumerable<CoinToStore> Read()
        {
            using (var connection = new SqlConnection(_connectionString)) {
                connection.Open();

                var adapter = new SqlDataAdapter(_getBalanceProcName, connection);
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;

                var dataSet = new DataSet();
                adapter.Fill(dataSet);
                var dataTable = dataSet.Tables[0];

                var coins = new List<CoinToStore>(dataTable.Rows.Count);

                for (int i = 0; i < dataTable.Rows.Count; ++i) {
                    coins.Add(new CoinToStore(
                        dataTable.Rows[i]["ExchangeName"].ToString(),
                        dataTable.Rows[i]["CoinTitle"].ToString(),
                        Convert.ToDouble(dataTable.Rows[i]["Amount"])
                    ));
                }

                return coins;
            }
        }

        public void Write(IEnumerable<CoinToStore> coins)
        {
            ClearBalance();
            using (var connection = new SqlConnection(_connectionString)) {
                connection.Open();

                SqlCommand cmd = new SqlCommand(_sqlInsertExpression, connection);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter p1 = cmd.CreateParameter();
                p1.DbType = DbType.Int32;
                p1.ParameterName = "@exchangeId";

                SqlParameter p2 = cmd.CreateParameter();
                p2.DbType = DbType.Int32;
                p2.ParameterName = "@coinId";

                SqlParameter p3 = cmd.CreateParameter();
                p3.DbType = DbType.Decimal;
                p3.ParameterName = "@amount";

                cmd.Parameters.Add(p1);
                cmd.Parameters.Add(p2);
                cmd.Parameters.Add(p3);

                foreach (var coin in coins) {
                    p1.Value = GetExchangeIdByName(coin.ExchangeName);
                    p2.Value = GetCoinIdByTitle(coin.Title);
                    p3.Value = coin.Amount;

                    cmd.ExecuteNonQuery();
                }
            }
        }

        private void ClearBalance()
        {
            var sqlExpression = "TRUNCATE TABLE Balance";
            using (SqlConnection connection = new SqlConnection(_connectionString)) {
                connection.Open();

                SqlCommand command = new SqlCommand(sqlExpression, connection);

                // TODO: catch exception
                command.ExecuteNonQuery();
            }
        }

        private Int32 GetExchangeIdByName(String name)
        {
            var sqlExpression = "SELECT Id FROM Exchange WHERE Name LIKE @exchangeName";
            using (SqlConnection connection = new SqlConnection(_connectionString)) {
                connection.Open();

                SqlCommand command = new SqlCommand(sqlExpression, connection);
                command.Parameters.AddWithValue("@exchangeName", name);

                // TODO: catch exception
                return (Int32)command.ExecuteScalar();
            }
        }

        private Int32 GetCoinIdByTitle(String title)
        {
            var sqlExpression = "SELECT Id FROM Coin WHERE Name LIKE @coinTitle";
            using (SqlConnection connection = new SqlConnection(_connectionString)) {
                connection.Open();

                SqlCommand command = new SqlCommand(sqlExpression, connection);
                command.Parameters.AddWithValue("@coinTitle", title);

                // TODO: catch exception
                return (Int32)command.ExecuteScalar();
            }
        }
    }
}